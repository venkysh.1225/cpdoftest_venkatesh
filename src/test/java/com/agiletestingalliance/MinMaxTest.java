package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;


public class MinMaxTest {

        @Test
        public void testCompare() throws Exception {
				MinMax minMax = new MinMax();
                int first = minMax.compare(2,5);
		assertEquals("MinMax", 5, first);
				int second = minMax.compare(5,2);
				assertEquals("MinMax1", 5, second);
        }
		
		
}

