package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;


public class AboutCPDOFTest {

        @Test
        public void testDesc() throws Exception {
                String output = new AboutCPDOF().desc();
		assertTrue("AboutTest", output.contains("CP-DOF"));
        }
}

